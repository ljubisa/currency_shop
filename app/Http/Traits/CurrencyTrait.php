<?php
namespace App\Http\Traits;

use App\Models\Currency;
use App\Models\ExchangeRate;

trait CurrencyTrait {

    /**
     * @param string $code
     * @return null
     */
    public function getExchangeRateDataForCode(string $code)
    {
        $currency = Currency::where('code', $code)->first();
        if(!empty($currency)) {
            return  ExchangeRate::select('id','exchange_rate', 'surcharge')->where('to_currency_id', $currency->id)->first();
        }
        return null;
    }

    /**
     * @param string $code
     * @param float $amount
     * @return float
     */
    public function calculatePrice(string $code, float $amount)
    {
        $result = 0;
        if(empty($code) || empty($amount)) {
            return $result;
        }
        $exchangeRateData = $this->getExchangeRateDataForCode($code);
        if(empty($exchangeRateData)){
            return null;
        }
        $result =  ($amount / $exchangeRateData->exchange_rate) + $exchangeRateData->surcharge;
        return number_format((float) $result, 2, ',', '.');
    }
}
