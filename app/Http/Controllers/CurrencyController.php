<?php

namespace App\Http\Controllers;

use App\Http\Traits\CurrencyTrait;
use App\Models\Currency;
use Illuminate\Http\Response;

class CurrencyController extends Controller
{

    use CurrencyTrait;
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function loadCurrencies(): Response
    {
        $currencies = Currency::where('default','=', false)->get();
        return response($currencies, Response::HTTP_OK);
    }

    /**
     * @param string $code
     * @return Response
     */
    public function getExchangeRateData(string  $code): Response
    {
        $exchangeRateData = $this->getExchangeRateDataForCode($code);
        if(empty($exchangeRateData)){
            return response('Exchange rate not found', Response::HTTP_NOT_FOUND);
        }
        return response($exchangeRateData, Response::HTTP_OK);
    }
}
