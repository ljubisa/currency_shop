<?php

namespace App\Http\Controllers;

use App\Http\Traits\CurrencyTrait;
use App\Models\Currency;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class OrderController extends Controller
{
    use CurrencyTrait;

    /**
     * @param Request $request
     * @return Response
     */
    public function store(Request $request): Response
    {
        $request->validate([
            'full_name' => 'required|max:120',
            'email' => 'required|email',
            'currency' => 'required|max:3',
            'amount' => 'required|numeric',
        ]);
        $code = $request->input('currency');
        $amount = $request->input('amount');
        $currency = Currency::where('code', $code)->first();
        $exchangeRateData = $this->getExchangeRateDataForCode($code);
        $price = $this->calculatePrice($code, $amount);

        $newCurrency = Order::create([
            'currency_id' => $currency->id,
            'exchange_rate_id' => $exchangeRateData->id,
            'amount' => $amount,
            'price' => $price,
            'surcharge' => $exchangeRateData->surcharge,
            'email' => $request->input('email'),
            'full_name' => $request->input('full_name')
        ]);

        if(!empty($newCurrency)) {
            return response('Your order was successfully submitted', Response::HTTP_CREATED);
        }
    }

}
