<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public $fillable = ['currency_id', 'exchange_rate_id', 'amount', 'price', 'surcharge', 'email', 'full_name'];
}
