<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ExchangeRate extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $fillable = ['from_currency_id', 'to_currency_id', 'exchange_rate', 'surcharge'];

    public function from_currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    public function to_currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }
}
