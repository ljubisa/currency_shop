<?php

use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\OrderController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/currencies', [CurrencyController::class,'loadCurrencies']);
Route::get('/calculate-price/{code}/{amount}', [CurrencyController::class,'calculatePrice']);
Route::get('/exchange-rate/{code}', [CurrencyController::class,'getExchangeRateData']);
Route::post('/order', [OrderController::class,'store']);

