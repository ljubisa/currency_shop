# Currency Shop
### Laravel test app

This is a simple one-page app that will allow a user to purchase foreign
currencies like Japanese Yen (JPY), British Pound (GBP) or Euro (EUR) and to pay in USD.

![](public/images/desktop.png)

## Instalation

- Clone the project from Github in your folder
  ```shell
   git clone git@bitbucket.org:ljubisa/currency_shop.git
  ```
- Run composer install from the root of a project
```shell
 composer install
```
- Inn app root, create a ".env" file and set up your database credentials
- In order to run migrations and seeders run the next commands in your terminal
```shell
php artisan migrate
```

```shell
php artisan db:seed
```
## About
For making a FE design prototype I was using Figma
https://www.figma.com/file/3gdzWb81hbfYkoC6p9WJp3/MENU-PROJECT

My idea was to make this page responsive using Bootstrap as a frontend framework

![responsive page](public/images/responsive.png)

For a database I was using the MySql relation database with the next model:

![db model](public/images/dbmodel.png)

and for javascript, I was using vanilla javascript and Jquery