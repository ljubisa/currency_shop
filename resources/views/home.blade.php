<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="/logo-small.png" type="image/x-icon">
    <title>Buy Currency</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="theme.css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css">
    <script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous"></script>
</head>
<body class="antialiased">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 d-flex col-left justify-content-center">
            <table>
                <tr>
                    <td>
                        <h1>Need to buy some currency?</h1>
                    </td>
                </tr>
                <tr>
                    <td>

                        <p>
                            Just choose the currency you want to buy and we will offer you the best prices
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="./logo.png" alt="currency shop">
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-lg-6 d-flex col-right justify-content-center">
            <div class="panel">
                <h2>Buy Currency</h2>
                <hr>
                <div class="panel-body">
                    <p>
                        Cugrayrrency I want to buy:
                    </p>
                    <div class="row">
                        <div class="col-lg-9">
                            <input class="form-control" onkeyup="calculatePrice()" id="amount" type="number" placeholder="0.00"
                                   aria-label="default input example">
                        </div>
                        <div class="col-lg-3">
                            <div class="btn-group">
                                <button class="btn btn-default dropdown-toggle" type="button" id="defaultDropdown"
                                        data-bs-toggle="dropdown" data-bs-auto-close="true" aria-expanded="false">
                                    <img id="selected-currency" src="./logo-small.png" alt="Select ...">
                                </button>
                                <ul class="dropdown-menu" id="currency-dropdown" aria-labelledby="defaultDropdown">

                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div id="result" style="display: none">
                                = <span id="price"></span> USD
                            </div>
                        </div>

                        <div class="col-lg-11">
                            <div class="row" id="fee">
                                <table id="currency-info">
                                    <tr>
                                        <td width="20%">Exchange rate:</td>
                                        <td width="50%">
                                            <hr>
                                        </td>
                                        <td width="30%" align="right">1.00 USD = <span id="exchange-rate"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Fee:</td>
                                        <td>
                                            <hr>
                                        </td>
                                        <td><span id="surcharge"></span> USD</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="col-lg-12 d-flex justify-content-center">
                            <button type="button" id="btn-buy"  class="btn btn-lg" data-bs-toggle="modal"
                                    data-bs-target="#exampleModal"><i class="fa fa-cart-plus"
                                                                      aria-hidden="true"></i> Buy now
                            </button>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Contact info</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div id="validation-errors">

                </div>
                <div class="mb-3">
                    <label for="full-name" class="form-label">Full name *</label>
                    <input type="text" class="form-control" id="full-name"
                           placeholder="">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email address *</label>
                    <input type="email" class="form-control" id="email"
                           placeholder="">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="submitOrder()">Submit</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="baseUrl" value="{{  url('') }}">
<script src="./app.js"></script>
</body>
</html>
