const baseUrl = document.getElementById('baseUrl').value;
let currencies = [];
let selectedCurrencyCode = null;
let exchangeRateData = null;

const selectCurrency = (currency) => {
    selectedCurrencyCode = currency;
    const selectedCurrencyHolder = document.getElementById('selected-currency');
    const exchangeRateHolder = document.getElementById('exchange-rate');
    if (selectedCurrencyCode === 'JPY') {
        selectedCurrencyHolder.src = './flags/japan.png'
    } else if (selectedCurrencyCode === 'GBP') {
        selectedCurrencyHolder.src = './flags/united_kingdom.png'
    } else if (selectedCurrencyCode === 'EUR') {
        selectedCurrencyHolder.src = './flags/european_union.png'
    }
    exchangeRateHolder.innerHTML = selectedCurrencyCode;
    getExchangeRateData();
}

const getExchangeRateData = () => {
    if (!selectedCurrencyCode) {
        console.log('currency is not selected!')
        return;
    }
    $.get(baseUrl + "/api/exchange-rate/" + selectedCurrencyCode, function (data, status) {
        if (data.length <= 0) {
            console.log('There are no exchange rate for selected currency');
        }
        exchangeRateData = data;
        calculatePrice();
        currencyInfo();
    });
}

const currencyInfo = () => {
    let currencyInfoHolder = document.getElementById('currency-info');
    if (selectedCurrencyCode) {
        currencyInfoHolder.style.display = "block";
        document.getElementById('exchange-rate').innerHTML = exchangeRateData.exchange_rate + ' ' + selectedCurrencyCode;
        document.getElementById('surcharge').innerHTML = exchangeRateData.surcharge;
    } else {
        currencyInfoHolder.style.display = "none";
    }
}

const calculatePrice = () => {
    let amount = document.getElementById('amount').value;
    let priceHolder = document.getElementById('price');
    let priceContainer = document.getElementById('result');
    if (amount <= 0) {
        priceContainer.style.display = "none";
    } else {
        if (!selectedCurrencyCode) {
            console.log('currency is not selected!')
            return;
        }
        $.get(baseUrl + "/api/calculate-price/" + selectedCurrencyCode + '/' + amount, function (data, status) {
            if (data.length <= 0) {
                console.log('There are no exchange rate for selected currency');
            }
            priceContainer.style.display = "block";
            priceHolder.innerHTML = data;
        });
    }
}

const loadCurrencies = () => {
    if (currencies.length > 0) {
        return
    }
    $.get(baseUrl + "/api/currencies", function (data, status) {
        if (data.length <= 0) {
            console.log('There are no currencies in database');
        }
        currencies = data;
        const options = currencies.map(currency => {
            return `<li>
                     <a class="dropdown-item" onclick="selectCurrency('${currency.code}')" href="#">
                        <img src="${currency.image}">${currency.name}
                        <span>(${currency.code})</span>
                     </a>
              </li>
            `
        });
        document.getElementById('currency-dropdown').innerHTML = options.toString().replaceAll(',', '');
    });
}

const submitOrder = () => {
    let fullName = document.getElementById('full-name').value;
    let email = document.getElementById('email').value;
    let amount = document.getElementById('amount').value;
    if (!amount) {
        alert('You have to insert your amount that you want to buy');
        return
    }
    if (!selectedCurrencyCode) {
        alert('You have to select a currency');
        return
    }
    if (!fullName) {
        alert('You have to insert your full name');
        return
    }
    if (!email) {
        alert('You have to insert your email');
        return
    }

    if (!fullName || !email || !amount || !selectedCurrencyCode) {
        alert('You have to insert your full name');
        return
    }

    if (!validEmail(email)) {
        alert('Invalid email');
        return
    }

    let postData = {
        'full_name': fullName,
        'email': email,
        'currency': selectedCurrencyCode,
        'amount': amount
    }

    $.post(baseUrl + '/api/order', postData)
        .done(function (msg) {
            document.getElementById('full-name').value='';
            document.getElementById('email').value='';
            document.getElementById('amount').value='';
            $("#exampleModal").modal('hide');
            alert(msg);
        })
        .fail(function (xhr, status, error) {
            $('#validation-errors').html('');
            $.each(xhr.responseJSON.errors, function (key, value) {
                $('#validation-errors').append('<div class="alert alert-danger">' + value + '</div');
            });
        });
}
document.addEventListener("DOMContentLoaded", function () {
    loadCurrencies();
});


const validEmail = (email) => {
    return email.match(/^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-z0-9]{1}[a-z0-9\-]{0,62}[a-z0-9]{1})|[a-z])\.)+[a-z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$/i);
}
