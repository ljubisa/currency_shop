<?php

namespace Database\Seeders;

use App\Models\ExchangeRate;
use Illuminate\Database\Seeder;

class ExchangeRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            [
                'from_currency_id' => '1',
                'to_currency_id' => '2',
                'exchange_rate' => '107.17',
                'surcharge' => '7.5',
            ],
            [
                'from_currency_id' => '1',
                'to_currency_id' => '3',
                'exchange_rate' => '0.711178',
                'surcharge' => '5',
            ],
            [
                'from_currency_id' => '1',
                'to_currency_id' => '4',
                'exchange_rate' => '0.884872',
                'surcharge' => '5',
            ]
        ];

        if (ExchangeRate::all()->count()) {
            return;
        }
        foreach ($records as $record) {
            ExchangeRate::create($record);
        }
    }
}
