<?php

namespace Database\Seeders;

use App\Models\Currency;
use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            [
                'code' => 'USD',
                'name' => 'United States Dollar',
                'symbol' => '$',
                'image' => './flags/usa.png',
                'default' => true
            ],
            [
                'code' => 'JPY',
                'name' => 'Japanese Yen',
                'symbol' => '¥',
                'image' => './flags/japan.png',
                'default' => false
            ],
            [
                'code' => 'GBP',
                'name' => 'British Pound',
                'symbol' => '£',
                'image' => './flags/united_kingdom.png',
                'default' => false
            ],
            [
                'code' => 'EUR',
                'name' => 'Euro',
                'symbol' => '£',
                'image' => './flags/european_union.png',
                'default' => false
            ]
        ];
        if (Currency::all()->count()) {
            return;
        }
        foreach ($currencies as $currency) {
            Currency::create($currency);
        }
    }
}
